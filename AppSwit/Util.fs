﻿[<AutoOpen>]
module Util
open FStap open System open System.Windows open System.Windows.Controls
open System.Diagnostics
open System.Windows.Input
open System.Windows.Interop
open System.Windows.Media.Imaging


type Process with
  member __.hMain = __.MainWindowHandle
  member __.title = __.MainWindowTitle
  member __.name  = __.MainModule.FileName
  member __.activate = 
    ShowWindow(__.hMain, SW_SHOWNORMAL).ig
    SetForegroundWindow(__.hMain).ig


let wnd = Window' "AppSwit" 
wnd.[ WindowStyle.None ]

let Keys = [
  [Key.D1;Key.D2;Key.D3;Key.D4;Key.D5;Key.D6]
  [Key.Q; Key.W; Key.E; Key.R; Key.T; Key.Y]
  [Key.A; Key.S; Key.D; Key.F; Key.G; Key.H]
  [Key.Z; Key.X; Key.C; Key.V; Key.B; Key.N] ]

let KeyEvents = Dic<Key, u->u>()


// funcs
let kToS (key:Key) =
  let k = key.s
  if k.isMat "D\d" then k.[1..1] else k

let getWindows<'__> = 
  Process.GetProcesses() 
  >%? fun proc -> proc.MainWindowHandle <> 0n
  
let getIcon path =
  let ico = Drawing.Icon.ExtractAssociatedIcon path
  Imaging.CreateBitmapSourceFromHIcon(
    ico.Handle,
    r32 0 0 ico.Width ico.Height,
    BitmapSizeOptions.FromEmptyOptions() )

let shadowed (co:s) sz s = 
  ShadowedLabel(sz, co, s, FontWeights.ExtraBlack)


let ini = wnd.ig