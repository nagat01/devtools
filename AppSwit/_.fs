open FStap open System open System.Windows open System.Windows.Controls open System.Reflection
[<product "AppSwit"; version "0.0"; company "ながと"; copyright "Copyright © ながと 2014";
  title "AppSwit: __"; STAThread>] SET_ENG_CULTURE


// ini
Util.ini
KeyPanel.ini
KeyOperations.ini


// ui
let _ = KeyPanel.sp $ wnd


// funcs
let showKeyPanel<'__> =
  pref.save
  KeyPanel.sp |>* wnd 
  KeyPanel.renew 


// events
btnPref.mdown =+ { Settings.dp |>* wnd }
Settings.btnBack =+ { showKeyPanel }

wnd.MouseEnter -%% true >=< wnd.activatedChanged => fun b -> 
  immediate' {
  showKeyPanel
  if b then
    do! 20
    wnd |> szToCtt
    do! 20
    wnd.po <- po ((wscr - wnd.w) / 2.) ((hscr - wnd.h) / 2.)
  else
    wnd.minimize }


// init
trace true {
wnd.run
}