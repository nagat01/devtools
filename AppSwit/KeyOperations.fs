﻿[<AutoOpen>]
module KeyOperations
open FStap open System open System.Windows open System.Windows.Controls open System.Reflection
open System.Collections.Generic
open System.Windows.Input


let switchIfKeyPressed<'__> =
  if wnd.handle = GetForegroundWindow() && KeyPanel.isTop then
    for key in Keys.collect id do 
      KeyEvents.find key |%| fun f ->
        if Keyboard.IsKeyDown key then
          f ()
          KeyEvents.Clear()
          wnd.minimize


let mutable prev = false
let queue = Queue()
let bringForegroundIfPressedTwice<'__> =
  let b = Keyboard.IsKeyDown Key.LeftCtrl
  if prev <> b then
    prev <- b
    queue.enq now
    queue.truncate 4
    if now - queue.Peek() < msec 500 then
      wnd.toForeground
                 

loaded =+! {
while true do
  do! 10
  switchIfKeyPressed
  bringForegroundIfPressedTwice
}


let ini = queue.ig
