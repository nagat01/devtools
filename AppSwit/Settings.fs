﻿[<AutoOpen>]
module Settings
open FStap open System open System.Windows open System.Windows.Controls


let dp  = DockPanel()
let  btnBack = Button' "戻る" $ dp.top $ hcaCenter $ fsz 32 
let  sp = StackPanel() $ dp.top
for i, keys in Keys.seqi do
  let sp = HStackPanel() $ sp $ mgnl (i*24)
  for key in keys do
    let s = kToS key
    let p = pshs (sf "app_%s" s) ""
    let sp = StackPanel() $ sp $ mgn1 4
    let   _ = shadowed "c07f" 32. s $ sp $ zIdx 1 $ aTopLeft
    let   _ = TextBox' p.v $ sp $ twoWay p $ w 120 $ vaBottom
    ()
