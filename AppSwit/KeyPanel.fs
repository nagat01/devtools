﻿[<AutoOpen>]
module KeyPanel
open FStap open System open System.Windows open System.Windows.Controls


let KeyPanels = Dic()


// ui
let sp = StackPanel()
for i, keys in Keys.seqi do
  let sp = HStackPanel() $ sp $ mgnl (i*24)
  for key in keys do
    let s = kToS key
    let grid = Grid() $ sp $ size1 96 $ mgn1 4
    let   _  = shadowed "c07f" 32. s $ grid $ zIdx 1 $ vaTop $ haLeft
    let   dp = DockPanel() $ grid
    KeyPanels.[key] <- dp
  if i = 0 then
    btnPref $ size1 96 |>* sp
    

// funcs 
let isTop<'__> = 
  wnd.Content = upcast sp

type 'a Option with
  member __.pred f = match __ with Some x -> f x | _ -> false

let renew<'__> =
  for key in Keys.collect id do
    KeyPanels.[key].clear
  for proc in getWindows do 
    try
    let exe   = proc.name
    let name  = exe.nameNoExt
    let title = proc.title
    if name <> "sidebar" && name <> "Explorer" then
      let grid = Grid()  $ size1 96
      let  _   = Image() $ grid $ src (getIcon exe) 
      let  sp  = StackPanel()  $ grid $ vaBottom
      let  _   = shadowed "000" 10. title $ sp
      let  _   = shadowed "000" 10. name  $ sp
      grid.mdown =+ { proc.activate }
      Keys.collect(id).tryfind(fun k ->
        ( pref ? ("app_" + kToS k) ).is(fun _name -> name.hasLower _name && name.len <= _name.len))
      |%| fun key -> 
        grid |>* KeyPanels.[key]
        KeyEvents.[key] <- fun _ -> 
          proc.activate
    with _ -> ()


// ini
let ini = sp.ig 