open FStap
[<product "CutInternet"; version "0.0.0.0"; company "ながと"; copyright "Copyright © ながと 2014";
  title "CutInternet: disconnects the internet connection" >] ()

open System.Management
let disconnectInternet _ =
  let wmiQuery = SelectQuery "SELECT * FROM Win32_NetworkAdapter WHERE NetConnectionId != null AND Manufacturer != 'Microsoft' "
  use searcher = new ManagementObjectSearcher(wmiQuery)
  for item in searcher.Get () do
    (item :?> ManagementObject).InvokeMethod("Disable", null).ig 

disconnectInternet ()