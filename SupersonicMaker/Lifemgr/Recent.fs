﻿module Recent
open FStap open __ open System open System.Windows open System.Windows.Controls
open System.Windows.Shapes
open System.Windows.Threading

type RecentImage(span) as __ =
  inherit Grid()
  let img      = Image() $ __ 
  let  wb      = mkWriteableBitmap 720 40 $ img
  let polyline = Polyline() $ stroke 1. "77f" $ __
  do
  __ $ mgn1 2 $ w 640 |> ig
  tm10sec =+ {
  let hs = goodnessRecent.array.rev ++ arrayZero 721
  wb.writeRectXY (r32 0 0 720 40) <| fun x y ->
    if (x % 60 = 0 && y % 2 = 0) || (x % 2 = 0 && y = 20) then
      if x % 180 = 0 then 0xff777777 else 0xffcccccc
    else
      match hs.[x / span] with
      | "good"   -> 0xff77ff77
      | "bad"    -> 0xffffeeee 
      | "others" -> 0xffffffdd
      | _        -> 0xfff7f7f7
  let yo = 20. * 8. / 9.
  let pace = ref 0.
  polyline.Points.clear
  for i in min 720 ((goodnessRecent.len - 1) * span) .. -span .. 0 do
    let current = match hs.[i / span] with "good" -> -0.1 | "bad" -> 0.1 | _ -> 0.
    let x = (i + span) *. 640. / 720.
    let y = (1. + !pace) * yo
    po x y |>* polyline
    pace := within -1. 1. (!pace * (10. / 11.) + current)
  po 0 ((1. + !pace) * yo) |>* polyline
  }


type TimerLabel() as __ =
  inherit StackPanel()
  let mkLabel () = Lbl() $ fsz 24 $ w 80 $ hcaRight $ vcaCenter
  let mutable prev     = now
  let mutable ts       = Zero<TimeSpan>
  let mutable velocity = 0.
  let _timer = DispatcherTimer(Interval=sec 1)
  let lblTimer     = mkLabel() $ __
  let grid         = Grid()    $ __
  let  polygon     = Rectangle() $ grid $ haLeft 
  let  lblVelocity = mkLabel() $ grid $ vaCenter
  do 
  __ |> "000".bg
  _timer.Start()
  _timer -% { V Recorder.goodness } -?? (<>) "others" |> Observable.pairwise => fun (g0, g1) ->
    let time = min (sec 15.) (nowM &prev)
    let positive = If (velocity > 0.)
    ts <- if g0 = g1 then ts + time else Zero
    velocity <- velocity * (100. / 101.) + (match g1 with "good" -> 0.01 | "bad" -> -0.01 | _ -> 0.)
    lblTimer <*- ts.sShort false
    lblVelocity <*- (velocity * 120.).i
    lblTimer    |> (match g1 with "good" -> "7f7" | "bad" -> "f77" | _ -> "ccc").fg
    lblVelocity |> (positive "7f7" "f77").fg
    polygon.Width <- (abs velocity * 80.) 
    polygon.Height <- grid.rh
    polygon $ stroke 1. (positive "cfc" "fcc") $ fill (positive "c7f7" "cf77") |> ig


// ui
let sp          = HStackPanel()
let  lblTodo    = Label'()       $ sp
let  timerLabel = TimerLabel()   $ sp  
let  spRecent   = StackPanel()         $ sp
let   _         = RecentImage 10 $ spRecent
let   _         = RecentImage 1  $ spRecent

