﻿[<AutoOpen>]
module __.Main
open FStap open System open System.Windows open System.Windows.Controls
open System.Collections.Generic


( pwd +/ "history" ).mkDir


// main
let tm1sec = mkTimer 1000
let tm10sec = mkTimer 10000
let goodnessRecent = Queue<s>()


module CutInternet =
  let cutInternet = @"C:\P\DevTools\CutInternet\bin\Debug\CutInternet.exe"
  let btn = Button' (Icon.getImg cutInternet) $ w 80
  btn =+? { processPWD cutInternet }


let ini = me.ig