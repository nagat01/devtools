﻿[<AutoOpen>]
module __.Util
open FStap open __ open System open System.Windows open System.Windows.Controls
open System.Windows.Shapes


// time
type TimeSpan with
  member __.sMinute =
    match __.tmin.i with
    | GT 1440 ->"d'/'hh':'mm"
    | GT 600  -> "hh':'mm"
    | GT 60   -> "h':'mm"
    | GT 10   -> "''mm"
    | _       -> "''m"
    |> __.S

let sTime  (__:TimeSpan) = __.S "h':'mm" 
let sHours (__:f ) = TimeSpan.FromHours(__).S "h':'mm" 
let sDate  (__:DateTime) = __.S "MM月dd日"


// date
let today  = now.AddHours(-3.)
let sToday = sDate today

let sBeforeDays i = today.AddDays(- !.i) |> sDate

let week<'__> = [ for i in 0..6 -> sBeforeDays i ]


// wpf
let mkLine (co:s) = Line() $ stroke 1. co $ dotted [1.; 1.]

type ShadowedTime (time:TimeSpan) as __ =
  inherit HStackPanel() 
  let _ = ShadowedLabel(10., "0c0", time.sMinute)   $ __ $ mgnw 0
  let _ = ShadowedLabel(10., "fc0", time.S "':'ss") $ __ $ mgnw 0
  do __ |> mgnw 2

type ShadowedParcent (ratio:f, ?co:s) as __ =
  inherit 
    ShadowedLabel(10., co |? "aaa", sf "%.1f%%" (ratio * 100.)) 
  do __ |> mgnw 2

let update = tm10sec >< wnd.menter >< wnd.Activated >< wnd.Loaded >< wnd.Closing
