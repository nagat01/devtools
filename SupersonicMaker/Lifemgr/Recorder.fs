﻿[<AutoOpen>]
module __.Recorder
open FStap open __ open System open System.Windows open System.Windows.Controls


// history
let historyDay day   = Pref ("history" +/ day + ".json", wnd)
let historyWeek      = [| for day in week -> historyDay day |]
let historyToday     = historyWeek.[0]
let getHistoryWeek f = [| for __ in historyWeek -> __.kv.kv >%? (fst >> f) |]


// usage
let isCategory __ = "good bad others".words.have __
let isHistory (__:s) = __.isMat "\d+" 
let isSoftware __ = isCategory(__).n && isHistory(__).n

// goodness
let goodnessSoftware __ = 
  if   Pref.goodTime.ss.have __ then "good" 
  elif Pref.badTime.ss.have  __ then "bad"
  else "others"

let goodnessSite (__:s) = 
  let f (sites:s seq) = sites.any (fun site -> __.lower.Contains site.lower)
  if   f Pref.goodSites.ss then "good"
  elif f Pref.badSites.ss  then "bad"
  else "others"

let GoodnessCurrent<'__> =
  if goodnessRecent.len = 0 
  then "others"
  else goodnessRecent.rev.head 

let recentGoodness goodness = Seq.tryFindIndex ((=)goodness) goodnessRecent.rev.Tail
let RecentBad<'__>  = recentGoodness "bad"
let RecentGood<'__> = recentGoodness "good"
let goodnessLatest range = goodnessRecent.rev |> Seq.truncate range


// usage
let inline sortCategory __ = Seq.sortBy (fst >> function "good" -> 0 | "others" -> 1 | _ -> 2) __
let inline sortUsages   __ = Seq.sortBy (snd >> (~-)) __

let sUsages sep kvs = [ for k, v in sortUsages kvs -> k + " " + sTime v ].concat sep
let incUsage duration name = historyToday.F name (historyToday.f' name 0. + duration)
let getUsage pred sorter day = historyWeek.[day].kv.kv >%? (fst >> pred)  |%> (_f sec) |> sorter
let usages     day = getUsage isSoftware sortUsages day
let categories day = getUsage isCategory sortCategory day


// variables
let mutable prev = now
let mutable goodness = "others"


// funcs
let getHistory day time =
  let history =
    if day < 7 
    then historyWeek.[day]
    else sBeforeDays day |> historyDay
  (history.s' time "0,0").split ","
  |%> i.Parse
  |> function [|a; b|] -> (a, b) | _ -> (0, 0)

let incHistory isGood =
  let time = (prev - today.Date).tmin.i / 10 |> string 
  let good, bad = time |> getHistory 0 |> If isGood f_ _f ((+)1)
  historyToday.[time] <- sf "%d,%d" good bad


// events
tm10sec =+! {
// goodness
let mutable exe = fgExe.nameNoExt
goodness <- goodnessSoftware exe
do' {
let! browser = browserOf exe 
let! url     = topUrl "" browser
match goodnessSite(url + fgTitle) with
| "others" -> ()
| __ -> goodness <- __
exe <- goodness + " " + exe }
// usages
let duration = (nowM &prev).tsec
incUsage duration exe
incUsage duration goodness
// recent
match goodness with
| "good" -> incHistory true
| "bad"  -> incHistory false
| _      -> ()
goodnessRecent.enqTruncate goodness 720
// title
wnd.Title <- now.S "hh:mm " + sUsages " " (usages 0) 
}


let ini = incHistory.ig