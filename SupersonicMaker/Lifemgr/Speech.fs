﻿module Speech
open FStap open __


// func
let playChime dir =
  let dir = pwd +/ "mp3" +/ dir
  dir.files("*.mp3").rand
  |> me.playPath


// events
let every30min isGood (category:s) =
  tm1sec -% { yield (historyToday.f' category 0.).i / 300 } |> Observable.changed => fun t ->
    if isGood && t % 24 = 0 then changeTheme()
    let dir  = category + (t % 6 = 0).If "_chime" "_beep"
    if dir <> "good_beep" then playChime dir
every30min true  "good"
every30min false "bad"


tm10sec =+ {
let rate goodness range = (goodnessLatest range >%? (=) goodness).len
if GoodnessCurrent = "bad" then
  RecentBad |%| fun bad ->
    let good = RecentGood |? 0
    if good < bad && bad < 30 then sf "%d0秒でbadに切り替わりました" bad |> speechPnl.Speak
  if rate "good" 180 < 60 then
    playChime "good_beep"
}


let ini = every30min.ig