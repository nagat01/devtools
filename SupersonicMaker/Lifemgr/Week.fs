﻿module Week
open FStap open __ open System open System.Windows open System.Windows.Controls


let  wp  = WrapPanel() $ w 720

update =+ {
wp.clear
for day in 0..6 do
  let data = [| for i in 6*3 .. 6*27 -> Recorder.getHistory day i.s |]
  LineChart(day, data) |>* wp
}