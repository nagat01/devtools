﻿[<AutoOpen>]
module __.Chart
open FStap open __ open System open System.Windows open System.Windows.Controls
open System.Windows.Shapes


// funcs
let toRadian degree = degree / 180. * Math.PI

let goodnessUsage (name:s) =
  if name.Contains "good " then "good"
  elif name.Contains "bad " then "bad"
  else 
  match name, goodnessSoftware name with
  | "good", _ | _, "good" -> "good"
  | "bad",  _ | _, "bad"  -> "bad"
  | _ -> "others"

let coGoodness (name:s) =
  match goodnessUsage name with
  | "good" -> "44f" | "bad" -> "fee" | _ -> "ccc"

let coGoodnessLight key =
  match goodnessUsage key with
  | "good" -> "77f" | "bad" -> "faa" | _ -> "ddd"


// charts
type PieChart (categories:seq<s*TimeSpan>) as __ =
  inherit Canvas()
  let sum    = (categories |%> _x).sumBy(fun __ -> __.thour)
  let offset = ref 0.
  let dict   = dict categories
  let total  = ((dict.find "good" |? Zero) + (dict.find "bad" |? Zero)).thour
  do
    for category, length in categories do
      let co        = coGoodness category
      let coParcent = coGoodnessLight category
      let degree x = x * 360. / sum
      let value   = length.thour
      let parcent = value / total
      let min     = degree !offset |> floor
      let max     = degree (!offset + value) |> ceil
      let mid     = (min + max) / 2.
      let poArc degree =
        let t = toRadian degree
        po (1. + sin t) (1. + cos t) *. 75.
      let poOrig   = po 75 75
      let poCenter = poInnerDiv poOrig (poArc (innerDiv min max 0.5)) 0.4
      let polygon  = Polygon() $ __ $ fill co 
      let spLabel  = StackPanel()    $ __ $ zIdx 1 $ (poCenter - vec 20 10).mv
      let  _       = ShadowedLabel(10., "000", category) $ spLabel
      let  _       = ShadowedTime length                 $ spLabel
      if category <> "others" then 
        ShadowedParcent(parcent,coParcent) |>* spLabel
      poOrig +++ ( [min .. max] |%> poArc )
      |%|* polygon
      offset := !offset + length.thour


type LinesTime (max:TimeSpan) as __ =
  inherit Grid()
  let max = max.tsec
  let diff =
    match max with
    | LT 300.   -> 60.
    | LT 600.   -> 120.
    | LT 1800.  -> 300.
    | LT 3600.  -> 600.
    | LT 7200.  -> 1200.
    | LT 10800. -> 1800.
    | LT 21600. -> 3600.
    | LT 36000. -> 7200.
    | LT 57600. -> 10800.
    | LT 86400. -> 14400.
    | _         -> 28800. 
  do
  for i in 0 .. (max / diff).i - 1 do
    let second = diff *. (i + 1)
    let format = 
      match second with 
      | GT 86400. -> "d'/'hh" | GT 36000. -> "hh':'mm" | _ -> "h':'mm"
    let time = sec(second).S format
    let _ = Line() $ __.addTo(col=i) $ xy2 0 0 0 210 $ stroke 0.2 "000" $ haRight
    let _ = ShadowedLabel(8., "ccc", time) $ __.addTo(col=i) $ trX 10 $ vaBottom $ haRight
    __.addColStar (diff / max)
  __.fillColStar
  

type BarChart(usages:seq<s*TimeSpan>) as __ =
  inherit Grid() 
  let usages = 
      usages 
      |> Seq.sortBy(fun (_,t) -> -t) 
      |> Seq.filteri(fun i (_,t) -> (i < 10 || t.tmin > !.10) && i < 14)
  do
  if usages.ok then
    let nrow = usages.len
    let max = usages |%> snd |> Seq.max
    let _       = __ $ wh 175 200
    let  ugrid1 = UGrid()       $ __ $ rows nrow
    let  _      = LinesTime max $ __
    let  ugrid2 = UGrid()       $ __ $ rows nrow
    let  _      = Border()      $ __ $ bdr 0.1 "000" $ vaStretch $ haStretch
    let sum = Seq.sum [ for name, time in usages do if goodnessUsage name <> "others" then yield time.thour ]
    for name, time in usages do
      let co = coGoodness name
      let coParcent = coGoodnessLight name
      let ratio = time /. max
      let parcent = time.thour / sum 
      let gridGage = Grid() $ ugrid1
      let  dp      = DockPanel() $ gridGage $ co.bg
      let grid     = Grid() $ ugrid2
      let  _       = Line() $ grid $ xy2 0 0 200 0 $ stroke 0.1 "000" 
      let  dp      = DockPanel() $ grid $ vaCenter
      let   _      = ShadowedLabel(10., "000", name)     $ dp.left
      let   sp     = StackPanel()                              $ dp.right $ w 40 
      let    _     = ShadowedParcent(parcent, coParcent) $ sp $ haRight
      let   _      = ShadowedTime time                   $ dp.right $ haRight
      gridGage.addColStar ratio
      gridGage.addColStar (1. - ratio)


type StackBarChart(data:seq<s*TimeSpan> seq) as __ =
  inherit UGrid()
  do
    __ $ wh 200 200 |> rows (data.len + 1)
    let sum = 
      let result = Dic()
      for data in data do
        for k, v in data do
          result.[k] <- (result.find k |? Zero) + v
      dict result.kv 
    let data = 
      [ for i, data in Seq.zip [0..6] data -> sBeforeDays i, dict data ]
      +++ ("１週間", sum)
    for title, data in data do 
      let total = ((data.find "good" |? Zero) + (data.find "bad" |? Zero)).thour
      let grid      = Grid()     $ __
      let  gridGage = Grid()     $ grid
      let  ug       = UGrid(c=6) $ grid
      let  sp       = StackPanel()     $ grid $ haRight $ mgnw 2
      let   lblDay  = ShadowedLabel(10., "fca", title) $ sp $ haRight
      let   spTime  = HStackPanel()    $ sp   $ haRight $ trY -3
      let  _        = Line()     $ grid $ xy2 0 0 200 0 $ stroke 0.2 "000"
      for i in 0..5 do
        Line() $ ug $ xy2 0 0 0 30 $ stroke 0.2 "000" |> haRight
      for i in 0..2 do
        let week = If (total > 18.)
        let key     = "good bad others".words.[i]
        let co      = coGoodness key
        let time    = data.find key |? Zero
        let thour   = time.thour
        let ratio   = thour / (week 126. 18.)
        let parcent = thour / total
        let dp = DockPanel() $ gridGage.addTo(col=i) $ co.bg
        if key <> "others" then 
          let coParcent = coGoodnessLight key
          let _ = ShadowedLabel(10., co, time.sMinute) $ spTime
          let _ = ShadowedParcent(parcent, coParcent)  $ spTime
          ()
        gridGage.addColStar ratio
      gridGage.fillColStar


type LineChart (i, data:seq<i*i>) as __ =
  inherit Canvas()
  let mutable pace = 0.
  let polygonBad   = Polygon()  $ __ $ stroke 1. "8fdd" $ fill "4fdd" 
  let polygonGood  = Polygon()  $ __ $ stroke 1. "80f0" $ fill "44f4"
  let polylinePace = Polyline() $ __ $ stroke 1. "777f"
  do
  __ |> wh 720 60
  // graph
  po 0   60 |>* polygonBad
  po 0   60 |>* polygonGood
  data |> Seq.iteri (fun i (good, bad) ->
    pace <- (pace / 1.25) +. good -. bad
    let pace = within -30. 30. (pace / 6.)
    po (i * 5) (60 -  bad ) |>* polygonBad
    po (i * 5) (60 -  good) |>* polygonGood
    po (i * 5) (30 -. pace) |>* polylinePace)
  po 720 60 |>* polygonBad
  po 720 60 |>* polygonGood
  // grid
  mkLine "2000" $ xy2 0 30 720 30 |>* __
  for x in 0 .. 30 .. 30 * 24 do
    If (x % 90 = 0) (Line() $ stroke 1. "3000") (mkLine "2000") 
    $ xy2 x 0 x 60 |>* __
  // time
  ShadowedLabel(13., "fca", sBeforeDays i) |>* __
  if i = 0 then
    for x in 3 .. 3 .. 21 do
      ShadowedLabel(10., "ccc", sf "%d:00" (x+3)) 
      $ mv (x * 30 - 15) 0 |>* __
