﻿module Software
open FStap open __ open System open System.Windows open System.Windows.Controls
open Chart


// util
let labels isKey (kvs:seq<s*f>) = 
  try [ for k,v in kvs -> If isKey k "" + " " + sHours v ] with _ -> []
 

// ui  
let sp               = HStackPanel()
let  sp1             = StackPanel() $ sp
let   nudDay         = Nud week $ sp1
let   lblPie         = Label'() $ sp1
let  lblSoftwareDay  = Label'() $ sp $ wh 180 210 $ ccenter
let  lblSoftwareWeek = Label'() $ sp $ wh 180 210 $ ccenter
let  lblGoodnessWeek = Label'() $ sp $ wh 200 210 $ ccenter


// events
nudDay >< update =+ {
let usagesWeek = 
  let dic = Dic<s,TimeSpan>()
  for s, ts in [0..6] >>= usages do
    dic.[s] <- (dic.find s |? Zero) + ts
  dic.kv
PieChart (categories nudDay.idx) $ size1 160 |>* lblPie
BarChart (usages nudDay.idx) |>* lblSoftwareDay
BarChart usagesWeek |>* lblSoftwareWeek
StackBarChart (getHistoryWeek isCategory |%> map (_f(f.Parse >> sec))) |>* lblGoodnessWeek
}