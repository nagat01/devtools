﻿[<AutoOpen>]
module __.RtbMenu
open FStap open System open System.Windows open System.Windows.Controls
open System.Windows.Documents
open System.Windows.Shapes


type ColorTip() as __ =
  inherit HStackPanel()
  let colors = [ 
    for co in "f00 0f0 00f 000 444 888 ccc fff".words ->
      Rectangle() $ fill co $ stroke 1 "fff" $ size1 20 $ __ ]
  do
    for co in colors do
      co.lclick =+ {
      for __ in colors do 
        stroke 1 (If (__ = co) "7f7" "fff") __ }
  member __.e = Observable.concat [ for co in colors -> co.lclick -%% co.Fill.co ]


type StyleButton< 'a when 'a :> Span and 'a : (new : u -> 'a) > (s:s) as __ =
  inherit Border()
  let b = sh false
  let _      = __ $ size1 20 
  let  tblk  = TextBlock () $ center $ __
  let   span = new 'a()
  do
    tblk.Inlines <+ span
    span.Inlines <+ s
    __.lmdown >< loaded =+ { 
      b <*- b.v.n
      __ $ bdr 0.5 (b.If "777" "000") |> (b.If "fff" "ddf").bg }
  member __.e = b.fired -% { V b.v.n }


type StyleTip() as __ =
  inherit HStackPanel()
  member val bold      = StyleButton<Bold     > "B" $ __
  member val italic    = StyleButton<Italic   > "I" $ __
  member val underline = StyleButton<Underline> "U" $ __


type HistoryTip(rtb:RichTextBox, tbx:TextBox') as __ =
  inherit Button' "history"
  do
    __ $ bdr 0.25 "000" |> mgnw 2
    __.lclick =+ {
      let tr   = TextRange(rtb.start, rtb.Selection.End)
      let app  = tr.Text.lines.rev.tryfind(fun s -> s.StartsWith("  ").n) |? "Unknown:"
      let line = sf "  %s (%A)" rtb.Selection.Text.trim now
      let text = tbx.Text
      match text.split app with
      | [|a; b|] -> a + app + "\r\n" + line + b
      | _        -> app + "\r\n" + line + "\r\n\r\n" + text 
      |>* tbx 
      rtb.Selection.Text <- "" }
    
