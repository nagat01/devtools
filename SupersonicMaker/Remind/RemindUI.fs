module __.RemindUI
open FStap open __ open System open System.Windows open System.Windows.Controls


// init
Pref.ini
Util.ini


// history
let icoNumHistory = IcoNum Ico.history
tbxHistory >< loaded =+ { tbxHistory |*> countTodo |>* icoNumHistory }


// main
let tabc    = TabControl() $ w 720 $ fun __ -> __.MinHeight <- 400.
let  spLife = StackPanel()     $ tabc.addHdrSel Ico.lifemgr
let  todo   = DirEdit (tabc, Ico.todo, "todo") 
let  memo   = DirEdit (tabc, Ico.memo, "memo")
let  _      = tbxHistory $ tabc.addHdrSel icoNumHistory
let  _      = ppnl       $ tabc.addHdrSel Ico.pref 


let focusedText () = opt {
  let! dirEdit  = tryCast<DirEdit> (tabc.item :?> TabItem).Content
  let! fileEdit = tryCast<FileEdit> dirEdit.item
  return fileEdit.name, fileEdit.selection.Or fileEdit.text }

// events
todo.menter =+? {
  todo.icoNum <*- query {
    for file in (pwd +/ "todo").files "*.txt" do
    where (file.Contains "unim")
    sumBy (countTodoXaml file)
    }
  }


let ini = tabc.ig