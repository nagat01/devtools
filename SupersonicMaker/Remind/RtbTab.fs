﻿[<AutoOpen>]
module __.RtbTab
open FStap open System open System.Windows open System.Windows.Controls
open System.IO
open System.Windows.Documents


type FileEdit (tabc:TabControl, file:s) as __ =
  inherit TabItem()
  do 
    __ |>* tabc
    __.menter =+ { tabc.item <- __ }
  let _name       = file.nameNoExt
  let rtb         = RichTextBox()
  let sp          = HStackPanel()    $ __.hdr
  let  lbl        = Lbl _name  $ sp
  let  btn        = Button' Ico.delete $ sp $ pad1 0 $ size1 16
  let dp          = DockPanel()     $ __
  let  spMenu     = HStackPanel()    $ dp.top
  let  colorTip   = ColorTip() $ spMenu
  let  styleTip   = StyleTip() $ spMenu
  let  historyTip = HistoryTip(rtb, tbxHistory) $ spMenu
  let  _          = rtb        $ dp.bottom
  do
    tabc       =+? { loadRtb rtb file }
    lbl.lclick =+  { rtb.text.lines |%| speechPnl.Speak }
    btn        =+  { tabc.rem __ }
    colorTip   => fun co -> 
      rtb.prop RichTextBox.ForegroundProperty (br co)
    styleTip.bold => fun b ->
      rtb.prop RichTextBox.FontWeightProperty (If b FontWeights.Bold FontWeights.Normal)
    styleTip.italic => fun b ->
      rtb.prop RichTextBox.FontStyleProperty (If b FontStyles.Italic FontStyles.Normal)
    styleTip.underline => fun b ->
      rtb.prop Inline.TextDecorationsProperty (If b TextDecorations.Underline null)
    rtb =+ { 
      saveRtb rtb file }
    rtb >< lbl.menter =+ { 
      lbl <*- sf "%s %d" _name (countTodo rtb.text) }
    
  member __.name      = _name
  member __.selection = rtb.Selection.Text
  member __.text      = rtb.text


type IcoNum(ico) as __ =
  inherit HStackPanel()
  let _   = ico |>* __
  let lbl = Label' "" $ __
  member __.v with set v = lbl.Content <- v


type DirEdit (tabc:TabControl, ico:Image, ext:s) as __ =
  inherit TabControl()
  let dir = pwd +/ ext
  do  dir.mkDir
  let mutable iPrev  = -1
  let mutable tmPrev = now
  let mkFile name = dir +/ (name + ".txt")
  let names = seq { for tabi:FileEdit in extract __.is -> tabi.name }
  let _icoNum = IcoNum ico
  do  
    tabc.addHdrSel _icoNum __
    ico.lclick =+? { processPWD dir }

  let tbxAddr     = TextBox'() $ w 70
  let _, tabiAddr = () |> __.addHdrSel' tbxAddr
  let add name =
    let tabi = FileEdit(__, mkFile name)
    tabiAddr $ __.rem $ __ |> ig
    tabi.mmove => fun e ->
      if lPressed then
        let iCur = __.is.idx((=)(box tabi))
        if iPrev <> -1 && iPrev <> iCur && now - tmPrev > msec 300 then
          tmPrev <- now
          __.swap iCur iPrev 
        iPrev <- iCur
      else 
        iPrev <- -1
  do
    pref.ss (ext + "_files") |%| add
    tbxAddr.autoComplete(
      ( fun _ -> dir.files "*.txt" |%> Path.GetFileNameWithoutExtension >%?~ names.have ), 
      ( fun _ ->
        if tbxAddr.Text.ok then
          add <-* tbxAddr
          tbxAddr <*- "") ,
      false)
    mw.Closing =+ { pref.Ss (ext + "_files") names }

  member __.icoNum = _icoNum