﻿[<AutoOpen>]
module __.Util
open FStap open System open System.Windows open System.Windows.Controls
open System.Windows.Documents
open System.Windows.Markup


// util
let loadRtb (rtb:RichTextBox) (file:s) =
  if file.fileOk then
    file.fileStreamOpen <| fun stream ->
      try' { rtb.range.Load(stream, DataFormats.Xaml) }

let saveRtb (rtb:RichTextBox) (file:s) =
  file.fileStreamSave <| fun stream ->
    rtb.range.Save(stream, DataFormats.Xaml)

let countTodo (s:s) = 
  s.lines.sumBy(fun line -> line.StartsWith("  ").If 1 0)

let countTodoXaml file =
  try
    let rtb = RichTextBox()
    loadRtb rtb file
    countTodo rtb.text
  with _ -> 
    0


// constants
let ColorsPalette = [ "000"; "333"; "666"; "999"; "ccc"; "f00"; "0f0"; "00f" ]
let history = pshs "history" ""


let tbxHistory = TextBox'() $ multiline $ v history.v $ depend history

let ini = tbxHistory.ig