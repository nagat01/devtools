﻿[<AutoOpen>]
module __.Pref
open FStap open __ open System.Windows.Controls


// init
Misc.ini


let ppnl       = StackPanel() $ w 720
let mk key def = PTbx (key, def) $ ppnl

let goodTime  = mk "goodTime"  ""
let badTime   = mk "badTime"   ""
let goodSites = mk "goodSites" ""
let badSites  = mk "badSites"  ""

let sliVol       = FloatVolume("音量", 1., 0.5) $ ppnl
let sliSpeechVol = IntVolume("読み上げの音量", 100, 15) $ ppnl
let speechPnl    = SpeechPanel(0.1, 10) $ ppnl


// events
sliVol >< sliSpeechVol >< loaded =+ { 
  me.Volume <- sliVol.v 
  speechPnl.Volume sliSpeechVol.v
  }

let ini = 
  ppnl.ig