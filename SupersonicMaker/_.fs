open FStap open __ open System open System.Windows open System.Windows.Controls open System.Reflection
[<product "SupersonicMaker"; version "0.3"; company "ながと"; copyright "Copyright © ながと 2014";
  title "SupersonicMaker: __"; STAThread>] SET_ENG_CULTURE


// init
LifemgrUI.ini
RemindUI.ini


// ui
let sp = StackPanel() $ wnd

let _  = me $ sp          
let _  = Recent.sp   $ sp
let _  = RemindUI.tabc $ sp
let  _ = Software.sp $ RemindUI.spLife
let  _ = Week.wp     $ RemindUI.spLife
 

// events
let mutable top    = 0.
let mutable height = 0.
wnd.activatedChanged => fun b ->
  RemindUI.tabc.Vis b
  if b then
    wnd.Top <- top
    wnd.Height <- height
    wnd.bordered
    Recent.lblTodo $ wh 0 0 |> ig
  else
    top <- wnd.Top
    height <- wnd.Height
    wnd.Top <- 0.
    wnd.Height <- Recent.sp.rh + 12.
    wnd.none
    RemindUI.focusedText() |%| fun (name, text) ->
      Recent.lblTodo $ wh 100 80 $ fsz 10 $ v (sf "%s:\r\n%s" name text) |> ig


trace true {

INIT_SHARED_VALUES
wnd.run

}

