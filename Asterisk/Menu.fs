﻿module __.Menu
open FStap open __ open System.Windows.Controls


open NAudio.CoreAudioApi
type AudioEndpointVolumeChannel with 
  member __.v 
    with get()     = float __.VolumeLevelScalar 
    and  set (v:f) = __.VolumeLevelScalar <- float32 v
let devices = MMDeviceEnumerator().GetDefaultAudioEndpoint(DataFlow.Render, Role.Multimedia).AudioEndpointVolume.Channels

module Commands =
  let btn = Button' Ico.theme $ caCenter $ size1 32
  btn => changeTheme

module Volume =
  let slider i = Slider'(0., 1., devices.[i].v, 50.) $ actv devices.[i].set_v $ vaCenter 
  let sp = HStackPanel() 
  let  _ = Label' Ico.speaker $ sp $ caCenter $ size1 32 
  let  sli0 = slider 0 $ sp
  let  sli1 = slider 1 $ sp
  let  btnSwap = Button' Ico.swap $ sp $ size1 32
  btnSwap =+ {
    let t = devices.[0].v 
    devices.[0].v <- devices.[1].v $ sli0
    devices.[1].v <- t $ sli1 }

let ini (add:StackPanel -> u) =
  let sp = HStackPanel() $ haCenter $ mgnb 4 $ "fff".bg $ add
  Commands.btn |>* sp
  Volume.sp |>* sp
  Button' icoPref $ caCenter $ size1 32 |>* sp 

