﻿[<AutoOpen>]
module __.Core
open FStap open System.Windows open System.Windows.Controls

let Ico = ImageProvider 24.
let timer = mkTimer 1000

type PrefPanel(pref, wnd, dp) as __ =
  inherit PrefPanelBase(pref, wnd, dp, icoPref.lclick.wait)
  member val themesRandom = __.s "themesRandom" "C:\\nagat01\\themes"
  member val dirsLauncher = __.s "dirsLauncher" "C:\\;C:\\P;C:\\R;C:\\I;C:\\nagat01;C:\\Users\\nagat01"
  member val exesLauncher = __.s "exesLauncher" "" 


let wnd = Window(Title = "Asterisk") $ h 480 
let  dp = DockPanel () $ wnd $ "bfd".bg
let pref = Pref("Asterisk", wnd.Closing.wait)
let  prefPanel = PrefPanel(pref, wnd, dp)

wnd |> restorePosSz pref
wnd.Loaded =+ { wToCtt wnd }


let unmat pats (ss:s seq) = ss.filter(fun s -> s.isMat(pats).n)

let solutions = @"C:\P".dirs

open System.IO
let isProject (dir:s) = 
  DirectoryInfo(dir).GetFiles("*.fsproj").Length > 0
let getProjects (sln:s) = seq {
  let filter (dir:s) = dir.dirs |> Seq.filter isProject
  yield! filter sln
  for dir in sln.dirs do
    yield! filter dir }

let projects = 
  let projects = Seq.cache <| seq { 
    for sln in solutions do
      yield! getProjects sln }
  projects

