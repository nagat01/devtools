﻿module Launcher
open FStap open __ open System.Windows.Controls
 

let slnFiles = [ for sln in solutions do for path in sln.files "*.sln" -> path, path.name ]
let slnDirs  = [ for sln in solutions -> sln, sln.name ]
let projDirs = [ for proj in projects -> proj +/ "bin" +/ "Debug", proj.name ]

let sp          = StackPanel() 
let  spComboBox = HStackPanel() $ sp $ "fff".bg $ mgnb 4 $ haCenter
let  spDirs = WrapPanel() $ sp $ w 560 $ "fff".bg $ mgnb 4 $ haCenter
let  spApps    = WrapPanel() $ sp $ w 560 $ "fff".bg $ mgnb 4 $ haCenter

let getExes = seq { 
  for proj in projects do
    yield! Seq.truncate 1 <| seq {
      let release = proj +/ @"bin\Release"
      let debug   = proj +/ @"bin\Debug"
      if release.dirOk then yield! release.files "*.exe"
      if debug.dirOk   then yield! debug.files   "*.exe" }
  yield! prefPanel.exesLauncher.ss }

let renewComboBoxes<'__> =
  spComboBox.clear
  mkComboBox slnFiles |>* spComboBox
  mkComboBox slnDirs  |>* spComboBox
  mkComboBox projDirs |>* spComboBox                                          

let renewLauncherDirs<'__> =
  spDirs.clear
  spDirs |> cs' (prefPanel.dirsLauncher.ss >%?? mkButton) 

let renewLauncherApps<'__> =
  spApps.clear
  spApps |> cs' (getExes >%?? mkButton) 

let ini (add:StackPanel -> u) =
  sp |> add
  renewComboBoxes
  renewLauncherDirs
  renewLauncherApps
  prefPanel.dirsLauncher =+ { renewLauncherDirs }
  prefPanel.exesLauncher =+ { renewLauncherApps }

