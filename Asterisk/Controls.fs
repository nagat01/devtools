﻿[<AutoOpen>]
module __.Controls
open FStap open System.Windows.Controls


let mkIconComboBox (path:s, name) = opt {
  do! path.pathOk
  let! ico = Icon.tryGetImg path
  let btn = DockPanel() $ w 180
  let  _  = ico         $ btn $ w 24
  let  _  = Label' name $ btn $ vcaCenter $ padw 2 $ fsz 20 
  btn.lmdown =+ { launch path }
  return btn }


let mkComboBox (paths:seq<s*s>) = 
  let cmb = ComboBox'() $ w 180 $ itemsi 0 (paths >%?? mkIconComboBox) $ vcaCenter
  let sw = Stopwatch()
  sw.Start()
  cmb.menter =+ {
    sw.Restart()
    cmb.IsDropDownOpen <- true }
  wnd.pmmove => fun e ->
    if sw.Elapsed.tmsec > 500. && (box cmb +++ cmb.is).have e.OriginalSource then 
      cmb.IsDropDownOpen <- false 
  cmb


let mkButton (path:s) = opt {
  let! ico = Icon.tryGetImg path
  do! (path.fileOk && path.ext = ".exe") || path.pathOk 
  let grid = Grid() $ w 72 $ mgn1 4
  let  _   = ico $ grid $ h 32 $ haLeft
  let  _   = ShadowedLabel(12., "070", path.nameNoExt) $ grid $ vaBottom 
  let btn  = Button'(grid, bg="fff") $ pad1 0
  btn =+ { launch path }
  return btn }


type LabeledTextBox (name:s, w:i) as __ =
  inherit HStackPanel ()
  let _    = Label' name $ __ $ pad2 4 2 $ bdr 1 "ccc" $ vaStretch $ hcaCenter $ fsz 20
  let _tbx = TextBox'()  $ __ $ bdr4 0 1 1 1 "ccc" $ WpfHelpers.w w $ fsz 20 $ wrap 
  member __.tbx = _tbx
  member __.v   = _tbx.Text

