﻿module Tools.UI
open FStap open __ open System.Windows.Controls
open System.IO


let ini(add:DockPanel -> u) =
  Core.ini add
  ILMerge.ini
  Git.ini
  Counter.ini

  tbxSln.tbx.autoComplete    (fun _ -> solutions |%> Path.GetFileName)
  tbxProj.tbx.autoComplete   (fun _ -> projects |%> Path.GetFileName)

