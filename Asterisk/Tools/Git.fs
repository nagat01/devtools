﻿module Tools.Git
open FStap open __

let push comment sln =
  let cmd = sfn "cd \"%s\"  \r\ngit add . -A  \r\ngit commit -m \"%s\"  \r\ngit push -u origin master" sln comment
  tbxRes.redirected (wnd, @"C:\Program Files\Git\bin\sh.exe", "--login -i", cmd)

let ini =
  tbxPush.enter    =+ { clear;  push tbxPush.v Sln;  tbxPush.tbx <*- "" }
  tbxPushAll.enter =+! { 
    clear
    "C:\P".dirs |%| push tbxPushAll.v }

