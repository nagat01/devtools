﻿module Tools.Counter
open FStap open __ open System 
open System.Text

let inline sb() = StringBuilder()

let (>>-) m f = 
  let mutable ls = 0 
  let mutable ws = 0 
  let mutable cs = 0 
  let mutable ss = sb()
  for l,w,c,s in m do 
    ls <- ls + l
    ws <- ws + w
    cs <- cs + c
    ss <- ss.Append(s:s)
  ls, ws, cs, f ls ws cs (ss.ToString())

let countFiles (dir:s) =
  let rec getFiles (dir:s):s seq =
    Seq.append
      (dir.dirs >>= getFiles) 
      (dir.files("*.fs|*.fsx|*.scala|*.coffee") 
        |> unmat "(ProvidedTypes\.fs|DebugProvidedTypes\.fs)")
  let mutable l = 0
  let mutable w = 0
  let mutable c = 0
  for file in getFiles dir do
    let text = file.read
    l <- l + text.lines.filter(fun s -> s.good).len
    w <- w + text.words.len
    c <- c + text.filter(Char.IsWhiteSpace>>not).len
  l, w, c, (sf "%s=%d " dir.name l)

let countProject (proj:s) =
  if isProject proj then [| proj |] else proj.dirs
  |%> countFiles >>- sfn "%-18s: %4d, %5d, %5d {  %s}" proj.name

let countSolution (sln:s): i*i*i*s =
  sln.dirs
  |> unmat "(packages|\.git|\.vs|target|blender|assets)" 
  |%> countProject >>- sfn "# %-16s: %4d, %5d, %5d\r\n%s" sln

let ini =
  cmb -? { is (cmb.item :?> s = "counter") } =+? {
    tbxRes <*- ""
    solutions
    |> unmat "(Research|Publish)" 
    |%> countSolution >>- tbxRes.tfn "ALL : %5d, %6d, %6d\r\n\r\n%s" |> ig 
    @"C:\P\Publish\files\count.txt".write tbxRes.Text }

