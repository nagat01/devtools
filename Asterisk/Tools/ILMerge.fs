﻿module Tools.ILMerge
open FStap open __
open System.Diagnostics


let config<'__> = btnConfig.Content :?> s

let ini =
  btnConfig =+ { btnConfig <*- (config = "Debug").If  "Release"  "Debug" }

  btnIlMerge =+! {

  clear
  let dir = 
    if "SakuSakuPaint" = Proj then @"C:\P\Paint\Paint.App\bin\Release\" 
    elif "MoshaTrainer" = Proj then @"C:\P\Training\MoshaTrainer\App\bin\Release\"
    elif "TwitDialog" = Proj then @"C:\P\WebTools\TwitDialog\App\bin\Debug\"
    else sf @"%s\%s\bin\%s\" (getSlnOfProj Proj) Proj config
  print "working dir is %s" dir
  let files = dir.files "*.dll|*.exe" >%?~ fun file -> file.name = "ffmpeg.exe"
  let exe = sf "%s%s.exe" dir Proj
  print "exe is %s" exe
  let ver    = match FileVersionInfo.GetVersionInfo(exe).FileVersion.TrimEnd('.','0') with "" -> "0" | __ -> __
  let outDir = sf @"C:\P\Secured\publish\%s" Proj
  let out    = sf @"/out:""%s\%s_v%s.exe""" outDir Proj ver
  let ndebug = (config = "Debug").If "/ndebug" ""
  let opts   = @"/allowDup /targetplatform:v4,""C:\Program Files\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.6"""
  let dlls   = exe +++ (files >%? (<>) exe) |%> tee (print "added %s") |> join " "
  outDir.mkDir  
  do! 100 
  tbxRes.redirected(
    wnd,
    @"C:\Program Files\Microsoft\ILMerge\ILMerge.exe",
    sf "%s %s %s %s" out ndebug opts dlls,
    dir=dir) }

