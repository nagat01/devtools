﻿[<AutoOpen>]
module Tools.Core
open FStap open __ open System.Windows.Controls


let dp    = DockPanel()
let  sp   = HStackPanel() $ dp.top $ "fff".bg
let   cmb = ComboBox'() $ sp $ itemsi 0 ["push all"; "push"; "il merge"; "counter"] $ fsz 20
let   tbxSln     = LabeledTextBox("sln",       70) $ sp
let   tbxProj    = LabeledTextBox("proj",      70) $ sp 
let   tbxPush    = LabeledTextBox("push",     200) $ sp
let   tbxPushAll = LabeledTextBox("push all", 200) $ sp
let   btnConfig  = Button' "Debug"   $ sp $ fsz 20    
let   btnIlMerge = Button' "ILMerge" $ sp $ fsz 20
let  tbxRes      = TextBox'() $ multiline $ equalWidth $ dp.bottom

let inline print fmt = tbxRes.tfn fmt
let clear<'__> = tbxRes <*- ""
let Sln<'__>   = @"C:\P" +/ tbxSln.v
let Proj<'__>  = tbxProj.v

let getSlnOfProj proj = query { 
  for sln in solutions do 
  where ( sln.Contains("Secured").n )
  find ( sln.dirs.exists(fun dir -> dir.name = proj) ) }

let collapse() = sp.cs >%? (<>)(cmb:>UIE) |%| collapsed

let ini (add:DockPanel -> u) =
  dp |> add
  collapse()

  wnd.Loaded >< cmb =+ {
  collapse()
  match cmb.item :?> s with                    
  | "push all"-> [ tbxPushAll ] : UIE list       
  | "push"    -> [ tbxSln; tbxPush ]
  | "il merge"-> [ tbxProj; btnConfig; btnIlMerge ]
  | _ -> [] 
  |%| vis 
  }

